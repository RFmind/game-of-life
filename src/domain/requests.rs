use domain::entities::World;

pub struct GenerateEmptyWorld {
    pub x: i32,
    pub y: i32
}

impl GenerateEmptyWorld {

    pub fn new(x: i32, y: i32) -> GenerateEmptyWorld {
        return GenerateEmptyWorld {x: x, y: y};
    }
}

pub struct GenerateDeadWorld {
    pub x: i32,
    pub y: i32
}


impl GenerateDeadWorld {
    
    pub fn new(x: i32, y: i32) -> GenerateDeadWorld {
        return GenerateDeadWorld{x:x, y:y};
    }
}

pub struct GenerateLivingWorld {
    pub x: i32,
    pub y: i32
}

impl GenerateLivingWorld {
    
    pub fn new(x: i32, y: i32) -> GenerateLivingWorld {
        return GenerateLivingWorld{x:x, y:y};
    }
}

pub struct GenerateRandomWorld {
    pub x: i32,
    pub y: i32
}

impl GenerateRandomWorld {
    
    pub fn new(x: i32, y: i32) -> GenerateRandomWorld {
        return GenerateRandomWorld{x:x,y:y};
    }
}

pub struct GenerateNextWorld {
    pub world: World
}

impl GenerateNextWorld {

    pub fn new(world: World) -> GenerateNextWorld {
        return GenerateNextWorld {world: world};
    }
}

pub struct GenerateCustomWorld {
    pub x: i32,
    pub y: i32,
    pub grid: Vec<Vec<bool>>
}

impl GenerateCustomWorld {
    
    pub fn new(x: i32, y: i32, grid: Vec<Vec<bool>>) -> GenerateCustomWorld {
        return GenerateCustomWorld {
            x: x,
            y: y,
            grid: grid
        }
    }
}
