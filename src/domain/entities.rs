extern crate rand;

use std::collections::HashMap;
use self::rand::Rng;

pub type WorldSize = (i32, i32);
type Location = (i32, i32);

pub struct Cell {
    pub alive: bool
}
impl Cell {
    
    pub fn alive() -> Cell {
        return Cell { alive: true };
    }

    pub fn dead() -> Cell {
        return Cell { alive: false };
    }
}

pub struct World {
    pub grid: HashMap<Location, Cell>,
    pub size: WorldSize
}

impl World {

    pub fn new_empty(worldsize: WorldSize) -> World {
        let world = World {
            grid: HashMap::new(),
            size: worldsize
        };
        return world;
    }

    pub fn new_living(worldsize: WorldSize) -> World {
        let mut world = World {
            grid: HashMap::new(),
            size: worldsize
        };
        for x in 0..worldsize.0 {
            for y in 0..worldsize.1 {
                world.grid.insert((x,y), Cell::alive());
            }
        }
        return world;
    }

    pub fn new_dead(worldsize: WorldSize) -> World {
        let mut world = World {
            grid: HashMap::new(),
            size: worldsize
        };
        for x in 0..worldsize.0 {
            for y in 0..worldsize.1 {
                world.grid.insert((x,y), Cell::dead());
            }
        }
        return world;
    }

    pub fn new_random(worldsize: WorldSize) -> World {
        let mut world = World {
            grid: HashMap::new(),
            size: worldsize
        };
        for x in 0..worldsize.0 {
            for y in 0..worldsize.1 {
                let cell = match rand::thread_rng().gen() {
                    true => Cell::alive(),
                    false => Cell::dead()
                };
                world.grid.insert((x,y), cell);
            }
        }
        return world;
    }

    pub fn as_vec(&self) -> Vec<Vec<bool>> {
        let mut vec_grid: Vec<Vec<bool>> = Vec::new();

        for i in 0..self.size.0 {
            let mut row: Vec<bool> = Vec::new();
            for j in 0..self.size.1 {
                let cell = self.grid.get(&(i,j)).unwrap();
                row.push(cell.alive);
            }
            vec_grid.push(row);
        }

        return vec_grid;
    }

    pub fn as_copy(&self) -> World {
        let mut new_world = World {
            grid: HashMap::new(),
            size: self.size
        };
        for x in 0..self.size.0 {
            for y in 0..self.size.1 {
                let cell = Cell {
                    alive: self.grid.get(&(x,y)).unwrap().alive
                };
                new_world.grid.insert((x,y), cell);
            }
        }
        return new_world;
    }

    pub fn neighbors(&self, location: &Location) -> Vec<Cell> {
        let mut cell_neighbors: Vec<Cell> = Vec::new();
        let &(x, y) = location;
        let neighbor_locations: Vec<(i32,i32)> = vec!(
            (-1, -1),
            (0 , -1),
            (1 , -1),
            (-1, 0),
            (1 , 0),
            (-1, 1),
            (0 , 1),
            (1 , 1)
        );

        for &(i,j) in neighbor_locations.iter() {
            if i < 0 && x == 0 {
                continue;
            }
            if j < 0 && y == 0 {
                continue;
            }
            if i > 0 && x+1 == self.size.0 {
                continue;
            }
            if j > 0 && y+1 == self.size.1 {
                continue;
            }

            let neighbor_location = (x+i, y+j);
            let neighbor = self.grid.get(&neighbor_location).unwrap();
            cell_neighbors.push(Cell{ alive: neighbor.alive });
        }

        return cell_neighbors;
    }
}

struct Rules {}
impl Rules {

    fn apply(cell: &Cell, neighbors: Vec<Cell>) -> (Cell, bool) {
        let mut alive_count = 0;
        for neighbor in &neighbors {
            if neighbor.alive {
                alive_count += 1;
            }
        }
        return match alive_count {
            0|1 => (Cell::dead(), true),
            2 => (Cell {alive: cell.alive}, false),
            3 => (Cell::alive(), !cell.alive),
            4...8 => (Cell::dead(), cell.alive),
            _ => (Cell {alive: cell.alive}, false)
        }
    }
}

pub struct Simulator {}
impl Simulator {

    pub fn next(world: World) -> (Option<World>, bool) {
        let mut new_world: World = World::new_empty(world.size);
        let mut world_changed: bool = false;

        for (location, cell) in world.grid.iter() {
            let neighbors = world.neighbors(location);
            let (new_cell, changed) = Rules::apply(cell, neighbors);
            new_world.grid.insert(*location, new_cell);

            if changed {
                world_changed = true;
            }
        }

        return (Some(new_world), world_changed);
    }
}
