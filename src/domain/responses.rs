use domain::entities::World;

pub struct WorldGenerated {
    pub world: World,
}
impl WorldGenerated {

    pub fn new(world: World) -> WorldGenerated {
        return WorldGenerated{ world: world };
    }
}

pub struct InputError {}
