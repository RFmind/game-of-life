use domain::entities::World;
use domain::entities::Simulator;
use domain::entities::WorldSize;
use domain::entities::Cell;
use domain::requests::GenerateEmptyWorld;
use domain::requests::GenerateNextWorld;
use domain::requests::GenerateDeadWorld;
use domain::requests::GenerateLivingWorld;
use domain::requests::GenerateRandomWorld;
use domain::requests::GenerateCustomWorld;
use domain::responses::WorldGenerated;
use domain::responses::InputError;

pub trait UseCases {

    fn generate_empty_world(&self, request: GenerateEmptyWorld)
        -> Result<WorldGenerated, InputError>;

    fn generate_dead_world(&self, request: GenerateDeadWorld)
        -> Result<WorldGenerated, InputError>;

    fn generate_living_world(&self, request: GenerateLivingWorld)
        -> Result<WorldGenerated, InputError>;

    fn generate_next_world(&self, request: GenerateNextWorld)
        -> Result<WorldGenerated, InputError>;

    fn generate_random_world(&self, request: GenerateRandomWorld)
        -> Result<WorldGenerated, InputError>;

    fn generate_custom_world(&self, request: GenerateCustomWorld)
        -> Result<WorldGenerated, InputError>;
}

pub struct GoLUseCases {}
impl UseCases for GoLUseCases {

    fn generate_empty_world(&self, request: GenerateEmptyWorld)
        -> Result<WorldGenerated, InputError> {
        
        let world_size: WorldSize = (request.x, request.y);
        let empty_world = World::new_empty(world_size);
        return Result::Ok(WorldGenerated::new(empty_world));
    }

    fn generate_dead_world(&self, request: GenerateDeadWorld)
        -> Result<WorldGenerated, InputError>{
        
        let world_size: WorldSize = (request.x, request.y);
        let dead_world = World::new_dead(world_size);
        return Result::Ok(WorldGenerated::new(dead_world));
    }

    fn generate_living_world(&self, request: GenerateLivingWorld)
        -> Result<WorldGenerated, InputError> {
        
        let world_size: WorldSize = (request.x, request.y);
        let living_world = World::new_living(world_size);
        return Result::Ok(WorldGenerated::new(living_world));
    }

    fn generate_next_world(&self, request: GenerateNextWorld)
        -> Result<WorldGenerated, InputError> {
    
        let (new_world, _) = Simulator::next(request.world);
        return match new_world {
            Some(world) => Result::Ok(WorldGenerated::new(world)),
            _ => Result::Err(InputError{})
        }
    }

    fn generate_custom_world(&self, request: GenerateCustomWorld)
        -> Result<WorldGenerated, InputError> {
    
        let worldsize: WorldSize = (request.x, request.y);
        let mut custom_world = World::new_empty(worldsize);

        for (i, row) in request.grid.iter().enumerate() {
            for (j, alive) in row.iter().enumerate() {
                let cell = match alive {
                    &true => Cell::alive(),
                    &false => Cell::dead()
                };
                custom_world.grid.insert((i as i32,j as i32), cell);
            }
        }

        return Result::Ok(WorldGenerated::new(custom_world));
    }

    fn generate_random_world(&self, request: GenerateRandomWorld)
        -> Result<WorldGenerated, InputError> {
        
        let worldsize: WorldSize = (request.x, request.y);
        let random_world = World::new_random(worldsize);
        return Result::Ok(WorldGenerated::new(random_world));
    }

}
impl GoLUseCases {
    
    pub fn new() -> GoLUseCases {
        return GoLUseCases{};
    }

}
