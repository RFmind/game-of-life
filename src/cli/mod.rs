extern crate rustbox;

use self::rustbox::{Color, RustBox};
use self::rustbox::Key;
use self::rustbox::Event::KeyEvent;

use domain::entities::World;
use domain::usecases::{UseCases, GoLUseCases};
use domain::requests::{
    GenerateLivingWorld,
    GenerateNextWorld,
    GenerateRandomWorld,
    GenerateCustomWorld};

use std::fs::File;
use std::io::prelude::*;

struct State {
    world: World,
    generation: i32
}

impl State {
    
    fn next(&self) -> State {
        let usecases = GoLUseCases::new();
        let request = GenerateNextWorld::new(self.world.as_copy());
        let result = usecases.generate_next_world(request);
        
        return match result {
            Ok(response) => State {
                world: response.world.as_copy(),
                generation: self.generation + 1
            },
            Err(_) => State {
                world: self.world.as_copy(),
                generation: self.generation
            }
        }
    }

}

struct FileView {
    data: String
}

impl FileView {
    
    fn from_file(path: String) -> FileView {
        let mut file = File::open(path)
            .expect("unable to open file");
        let mut datastring = String::new();
        file.read_to_string(&mut datastring)
            .expect("unable to read file");

        return FileView { data: datastring };
    }

    fn from_state(state: State) -> FileView {
        let mut datastring = String::new();

        for row in state.world.as_vec() {
            for alive in row {
                if alive {
                    datastring.push('#');
                } else {
                    datastring.push(' ');
                }
            }
            datastring.push(':');
        }

        return FileView { data: datastring };
    }

    fn to_file(&self, path: String) {
        let mut file = File::create(path)
            .expect("unable to create file");
        file.write_all(self.data.as_bytes());
    }

    fn as_vec(&self) -> Vec<Vec<bool>> {
        let mut grid: Vec<Vec<bool>> = Vec::new();
        let mut row: Vec<bool> = Vec::new();
        let mut y: i32 = 0;

        for (i, data_char) in self.data.chars().enumerate() {
            match data_char {
                ':' => {
                    grid.push(row);
                    row = Vec::new();
                    if y == 0 {
                        y = i as i32;
                    }
                },
                '#' => { row.push(true); },
                ' ' => { row.push(false); }
                _ => { }
            }
        }

        return grid;
    }
}

struct LinesView {
    data: Vec<String>
}

impl LinesView {

    fn from_state(state: &State) -> LinesView {
        let mut lines: Vec<String> = Vec::new();

        for row in state.world.as_vec() {
            let mut line = String::new();
            for alive in row {
                if alive {
                    line.push('#');
                } else {
                    line.push(' ');
                }
            }
            lines.push(line);
        }
        return LinesView { data: lines };
    }

}

struct CliApp {}
impl CliApp {

    fn run(state: State) {
        let rustbox = RustBox::init(Default::default())
            .ok().expect("failed to create rustbox instance");

        let mut current_state = state;
        CliApp::draw_state(&rustbox, &current_state);

        loop {
            match rustbox.poll_event(false) {
                Ok(KeyEvent(key)) => {
                    match key {
                        Key::Char('q') => { break },
                        Key::Char('n') => {
                            let nextstate = current_state.next();
                            CliApp::draw_state(
                                &rustbox,
                                &nextstate);
                            current_state = nextstate;
                        },
                        _ => { }
                    }
                },
                Err(e) => panic!("{}", e),
                _ => { }
            }
        }
    }

    fn draw_state(rustbox: &RustBox, state: &State) {
        let header = &format!(
            "Game of Life - Generation: {}",
            state.generation);
        let offset = 4;
        let linesview = LinesView::from_state(state);

        rustbox.print(
            1,1,
            rustbox::RB_BOLD,
            Color::Green,
            Color::Black,
            header);

        for (i, line) in linesview.data.iter().enumerate() {
            rustbox.print(
                1, i+offset,
                rustbox::RB_BOLD,
                Color::White,
                Color::Black,
                line);
        }

        rustbox.present();
    }
}

pub struct CliController {}
impl CliController {

    pub fn start_living_simulation() {
        let usecases = GoLUseCases::new();
        let request = GenerateLivingWorld::new(50,100);
        let response = usecases.generate_living_world(request)
            .ok().expect("failed to generate world");
        let state = State { world: response.world, generation: 1 };
        CliApp::run(state);
    }

    pub fn start_random_simulation() {
        let usecases = GoLUseCases::new();
        let request = GenerateRandomWorld::new(50,100);
        let response = usecases.generate_random_world(request)
            .ok().expect("failed generating world");
        let state = State { world: response.world, generation: 1 };
        CliApp::run(state);
    }

    pub fn render_next_fileview(in_filename: String, out_filename: String) {

        let usecases = GoLUseCases::new();
        
        let fileview = FileView::from_file(in_filename);
        let grid = fileview.as_vec();
        let x: i32 = grid.len() as i32;
        let y: i32 = grid[0].len() as i32;
        
        let request = GenerateCustomWorld::new(x,y,grid);
        let response = usecases.generate_custom_world(request)
            .ok().expect(&format!("Error generating custom world"));
        let state = State { 
            world: response.world.as_copy(),
            generation: 1
        };
        
        let next_fileview = FileView::from_state(state.next());
        next_fileview.to_file(out_filename);
    }
}
