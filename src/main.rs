extern crate clap;

use clap::{Arg, App, SubCommand};
use cli::CliController;

mod cli;
mod domain;

fn main() {
    let matches = App::new("Game of Life")
        .version("0.1")
        .author("Ruslan F. <rfmind@gmail.com>")
        .subcommand(SubCommand::with_name("cli")
                    .about("a cli GoL simulator")
                    .version("0.1")
                    .arg(Arg::with_name("new")
                         .short("n")
                         .long("new")
                         .help("Starts simulation with a living world"))
                    .arg(Arg::with_name("random")
                         .short("r")
                         .long("random")
                         .help("Starts simulation with a random world")))
        .subcommand(SubCommand::with_name("file")
                    .about("read from input_file, write to out_file")
                    .arg(Arg::with_name("from")
                         .short("f")
                         .long("from")
                         .takes_value(true)
                         .value_name("INPUT_FILE")
                         .required(true)
                         .help("input_file path"))
                    .arg(Arg::with_name("to")
                         .short("t")
                         .long("to")
                         .takes_value(true)
                         .value_name("OUTPUT_FILE")
                         .required(true)
                         .help("output_file path")))
        .get_matches();

    if let Some(ms) = matches.subcommand_matches("cli") {
        
        if ms.is_present("new") {
            CliController::start_living_simulation();
        } else if ms.is_present("random") {
            CliController::start_random_simulation();
        } else {
            CliController::start_living_simulation();
        }
    }

    if let Some(ms) = matches.subcommand_matches("file") {
        let from_filename = ms.value_of("from").unwrap().to_owned();
        let to_filename = ms.value_of("to").unwrap().to_owned();
        CliController::render_next_fileview(
            from_filename,
            to_filename);
    }
}
