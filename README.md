# Game-Of-Life CLI

This is a simple implementation of a Game Of Life (Conway) simulator.
It is written in Rust and uses some aspects of Clean Architecture.

The application uses the following crates:
- clap: argument parser
- rustbox: an implementation of termbox in rust
- rand: library for random number generation

## Building and running the application

Make sure you have rust and cargo installed on your system.

### Build

```
cargo build --release
```

### Run

Copy the binary that you've just build.
```
cp ./target/release/gol /usr/local/bin
```

You can now use `gol` command.

`gol --help` gives you very nice information about commands, flags etc.
`gol cli --random` starts the simulation with a random world. press `n` to go
to the next generation.


